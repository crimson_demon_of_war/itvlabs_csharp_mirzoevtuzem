﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_first
{
    class Program
    {
        static void Main(string[] args)
        {
            decimal nmb;

            Console.Write("Your number is: ");
            decimal.TryParse(Console.ReadLine(), out nmb);
            int answer = 0;

            for (int i = 0; i < 3; i++)
            {
                nmb = nmb % 1 * 10;
                answer += Convert.ToInt32(Math.Floor(nmb));
            }

            Console.WriteLine("Your answer is: " + answer);
            Console.ReadKey();
        }
    }
}
