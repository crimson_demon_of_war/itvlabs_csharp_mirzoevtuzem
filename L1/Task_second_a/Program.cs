﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_second_a
{
    class Program
    {
        static void Main(string[] args)
        {
            double cost, liter_per_hkm, cost_per_liter;
            int distance; string answer;

            Console.Write("Расстояние в км: ");             Int32.TryParse(Console.ReadLine(), out distance);
            Console.Write("Расход топлива (л/100км): ");    double.TryParse(Console.ReadLine(), out liter_per_hkm);
            Console.Write("Цена за литр бензина: ");        double.TryParse(Console.ReadLine(), out cost_per_liter);

            cost = distance / liter_per_hkm * cost_per_liter;
            answer = Convert.ToInt32(Math.Floor(cost)) + "руб. " + Convert.ToInt32(Math.Floor(cost * 100 % 100)) + "коп.";

            Console.WriteLine("\nЦена за поездку: " + answer);
            Console.ReadKey();
        }
    }
}
