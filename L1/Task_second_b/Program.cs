﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            double a, b, c, m, n, d;

            Console.WriteLine("Enter a, b, c, m, n, d");
            double.TryParse(Console.ReadLine(), out a);
            double.TryParse(Console.ReadLine(), out b);
            double.TryParse(Console.ReadLine(), out c);
            double.TryParse(Console.ReadLine(), out m);
            double.TryParse(Console.ReadLine(), out n);
            double.TryParse(Console.ReadLine(), out d);

            Console.WriteLine("Your answer is " + Convert.ToInt32( (m*n + d == 0) ^ ( a*d*d + b*d + c == 0 )));
            Console.ReadKey();
        }
    }
}