﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_second_c
{
    class Program
    {
        static void Main(string[] args)
        {
            double cost; string answer;
            Console.Write("Введите число: "); double.TryParse(Console.ReadLine(), out cost);

            answer = Convert.ToInt32(Math.Floor(cost)) + "руб. " + Convert.ToInt32(Math.Floor(cost * 100 % 100)) + "коп.";

            Console.WriteLine("\nДенежный формат: " + answer);
            Console.ReadKey();
        }
    }
}